package feature

import (
	"gitlab.com/steve12312/gitlab-operator/pkg/gitlab/internal/v1beta1"
)

var (
	ReplaceInternalGitalyWithPraefect = v1beta1.ReplaceInternalGitalyWithPraefect
	ConfigureCertManager              = v1beta1.ConfigureCertManager
)
