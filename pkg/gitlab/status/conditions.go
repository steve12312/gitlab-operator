package status

import (
	"gitlab.com/steve12312/gitlab-operator/pkg/gitlab"
)

const (
	ConditionInitialized gitlab.ConditionType = "Initialized"
	ConditionUpgrading   gitlab.ConditionType = "Upgrading"
	ConditionAvailable   gitlab.ConditionType = "Available"
)

const (
	PhasePreparing = "Preparing"
	PhaseRunning   = "Running"
)
