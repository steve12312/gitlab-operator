package gitlab

import (
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	"gitlab.com/steve12312/gitlab-operator/controllers/settings"
	"gitlab.com/steve12312/gitlab-operator/pkg/support/charts"
	"gitlab.com/steve12312/gitlab-operator/pkg/support/charts/populate"
)

func TestGitLab(t *testing.T) {
	settings.Load()
	_ = charts.PopulateGlobalCatalog(
		populate.WithSearchPath(settings.HelmChartsDirectory))

	RegisterFailHandler(Fail)
	RunSpecs(t, "GitLab Suite")
}
